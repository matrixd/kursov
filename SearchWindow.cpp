//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SearchWindow.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSearchForm *SearchForm;
//---------------------------------------------------------------------------
__fastcall TSearchForm::TSearchForm(TComponent* Owner)
	: TForm(Owner)
{
	radioButtons = true;
        owner = Owner;
}
//---------------------------------------------------------------------------
void __fastcall TSearchForm::buttonClick(TObject *Sender)
{
if(radioButtons){
	switch(this->RadioGroup->ItemIndex){
		case 0:
                        OrderManagmentForm->searchById(this->edit->Text.ToInt());
                        break;
                case 1:
                        OrderManagmentForm->searchByName(this->edit->Text);
                        break;
                case 2:
                        OrderManagmentForm->searchByModel(this->edit->Text);
                        break;
                case 3:
                        OrderManagmentForm->searchBySn(this->edit->Text);
                        break;
	}
}
delete this;
}
//---------------------------------------------------------------------------

void __fastcall TSearchForm::FormClose(TObject *Sender, TCloseAction &Action)
{
delete this;
}
//---------------------------------------------------------------------------

