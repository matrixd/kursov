//---------------------------------------------------------------------------

#ifndef CustomersWindowH
#define CustomersWindowH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include "MainWindow.h"
//---------------------------------------------------------------------------
class TClientsForm : public TForm
{
__published:	// IDE-managed Components
	TButton *new_bt;
	TButton *edit_bt;
	TButton *del_bt;
	TListView *ListView;
	void __fastcall new_btClick(TObject *Sender);
	void __fastcall edit_btClick(TObject *Sender);
	void __fastcall del_btClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
	__fastcall TClientsForm(TComponent* Owner);
	void refresh();
};
//---------------------------------------------------------------------------
extern PACKAGE TClientsForm *ClientsForm;
//---------------------------------------------------------------------------
#endif
