//---------------------------------------------------------------------------
#ifndef CustomerWindowH
#define CustomerWindowH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "Customer.h"
#include "MainWindow.h"
#include "OrderWindow.h"
//---------------------------------------------------------------------------
class TCustomerWindowForm : public TForm
{
__published:	// IDE-managed Components
	TEdit *editName;
	TButton *Save;
	TEdit *editPhone;
	void __fastcall SaveClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
	bool exists;
    Customer* customer;
public:		// User declarations
	__fastcall TCustomerWindowForm(TComponent* Owner);
	void setCustomer(Customer *link);
};
//---------------------------------------------------------------------------
extern PACKAGE TCustomerWindowForm *CustomerWindowForm;
//---------------------------------------------------------------------------
#endif
 
