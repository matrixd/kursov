//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "OrdersManage.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TOrderManagmentForm *OrderManagmentForm;
//---------------------------------------------------------------------------
__fastcall TOrderManagmentForm::TOrderManagmentForm(TComponent* Owner)
	: TForm(Owner)
{
	orders = new vector<Order>(*MainWindowForm->orders);
        showAll();
}
//---------------------------------------------------------------------------
void __fastcall TOrderManagmentForm::searchBtClick(TObject *Sender)
{
TSearchForm *form = new TSearchForm(this);
form->Show();
}

void TOrderManagmentForm::searchById(int id){
	delete orders;
	orders = new vector<Order>(*MainWindowForm->orders); 
	int index = MainWindowForm->searchByIdO(id, orders);
        this->ListView->Items->BeginUpdate();
        this->ListView->Items->Clear();
        this->ListView->Items->EndUpdate();
}

void TOrderManagmentForm::searchByName(AnsiString name){
        unsigned int index;
        for(int unsigned i = 0; i < MainWindowForm->customers->size(); i++){
        	int n  = MainWindowForm->customers->at(i).getName().Pos(name);
                if(n != -1) {
                	index = i;
                	break;
                }
        }
        delete orders;
        orders = new vector<Order>();
        for(int unsigned i = 0; i < MainWindowForm->orders->size(); i++){
        	if(MainWindowForm->orders->at(i).getCustomerId() == index){
                	orders->push_back(MainWindowForm->orders->at(i));
                }
        }
        showItems();
}

void TOrderManagmentForm::searchByModel(AnsiString model){
	orders = new vector<Order>();
	for(int unsigned i = 0; i < MainWindowForm->orders->size(); i++){
        	if(MainWindowForm->orders->at(i).getModel().Pos(model) != 0){
                	orders->push_back(MainWindowForm->orders->at(i));
                }
        }
        ShowMessage(orders->size());
        showItems();
}

void TOrderManagmentForm::searchBySn(AnsiString sn){
	delete orders;
	orders = new vector<Order>();
	for(int unsigned i = 0; i < MainWindowForm->orders->size(); i++){
        	if(MainWindowForm->orders->at(i).getSn().Pos(sn) != 0){
                	orders->push_back(MainWindowForm->orders->at(i));
                }
        }
        showItems();
}

void TOrderManagmentForm::showItem(Order order){
	TListItem * item;
        item = this->ListView->Items->Add();
        item->Caption = order.getId();
        item->SubItems->Add(TMainWindowForm::types[order.getType()]);
        item->SubItems->Add(order.getModel());
        item->SubItems->Add(order.getSn());
        item->SubItems->Add(TMainWindowForm::status[order.getStatus()]);
        item->SubItems->Add(order.getCustomerId());
        int custId = MainWindowForm->searchByIdC(order.getCustomerId(), MainWindowForm->customers);
        item->SubItems->Add(MainWindowForm->customers->at(custId).getName());
}
void TOrderManagmentForm::showItems(){
	this->ListView->Items->BeginUpdate();
        this->ListView->Items->Clear();
	for(int unsigned i = 0; i < orders->size(); i++){
        	showItem(orders->at(i));
        }
        this->ListView->Items->EndUpdate();
}
void TOrderManagmentForm::showAll(){
	delete orders;
	orders = new vector<Order>(*MainWindowForm->orders);
        showItems();
}

void TOrderManagmentForm::sortById(){
	int size = int(orders->size());
        Order *a_orders = new Order[size];
        for(int unsigned i = 1; i < size; i++){
        	a_orders[i] = orders->at(i);
        	Order tmp = a_orders[i];
                int j;
                for(j = i; j > 0 && tmp.getId() < a_orders[j-1].getId(); j--)
                	a_orders[j] = a_orders[j-1];
                a_orders[j] = tmp;
        }
        this->ListView->Items->BeginUpdate();
        this->ListView->Items->Clear();
        for(int unsigned i = 1; i < size; i++) showItem(a_orders[i]);
        this->ListView->Items->EndUpdate();
}
void TOrderManagmentForm::sortByStatus(){
	int size = int(orders->size());
        Order *a_orders = new Order[size];
        for(int unsigned i = 1; i < size; i++){
        	a_orders[i] = orders->at(i);
        	Order tmp = a_orders[i];
                int j;
                for(j = i; j > 0 && tmp.getStatus() < a_orders[j-1].getStatus(); j--)
                	a_orders[j] = a_orders[j-1];
                a_orders[j] = tmp;
        }
        this->ListView->Items->BeginUpdate();
        this->ListView->Items->Clear();
        for(int unsigned i = 0; i < size; i++) showItem(a_orders[i]);
        this->ListView->Items->EndUpdate();
}
void TOrderManagmentForm::sortByModel(){
	int size = int(orders->size());
        Order *a_orders = new Order[size];
        for(int unsigned i = 1; i < size; i++){
        	a_orders[i] = orders->at(i);
        	Order tmp = a_orders[i];
                int j;
                for(j = i; j > 0 && tmp.getModel() < a_orders[j-1].getModel(); j--)
                	a_orders[j] = a_orders[j-1];
                a_orders[j] = tmp;
        }
        this->ListView->Items->BeginUpdate();
        this->ListView->Items->Clear();
        for(int unsigned i = 0; i < size; i++) showItem(a_orders[i]);
        this->ListView->Items->EndUpdate();
}
void TOrderManagmentForm::sortBySn(){
	int size = int(orders->size());
        Order *a_orders = new Order[size];
        for(int unsigned i = 1; i < size; i++){
        	a_orders[i] = orders->at(i);
        	Order tmp = a_orders[i];
                int j;
                for(j = i; j > 0 && tmp.getSn() < a_orders[j-1].getSn(); j--)
                	a_orders[j] = a_orders[j-1];
                a_orders[j] = tmp;
        }
        this->ListView->Items->BeginUpdate();
        this->ListView->Items->Clear();
        for(int unsigned i = 0; i < size; i++) showItem(a_orders[i]);
        this->ListView->Items->EndUpdate();
}
void TOrderManagmentForm::sortByCustId(){
	int size = int(orders->size());
        Order *a_orders = new Order[size];
        for(int unsigned i = 1; i < size; i++){
        	a_orders[i] = orders->at(i);
        	Order tmp = a_orders[i];
                int j;
                for(j = i; j > 0 && tmp.getCustomerId() < a_orders[j-1].getCustomerId(); j--)
                	a_orders[j] = a_orders[j-1];
                a_orders[j] = tmp;
        }
        this->ListView->Items->BeginUpdate();
        this->ListView->Items->Clear();
        for(int unsigned i = 0; i < size; i++) showItem(a_orders[i]);
        this->ListView->Items->EndUpdate();
}
void TOrderManagmentForm::sortByDev(){
	ShowMessage("1");
	int size = int(orders->size());
	Order *a_orders = new Order[size];
	for(int i = 1; i < size; i++){
		a_orders[i] = orders->at(i);
		Order tmp = a_orders[i];
		int j;
		for(j = i; j > 0 && tmp.getType() < a_orders[j-1].getType(); j--)
			a_orders[j] = a_orders[j-1];
		a_orders[j] = tmp;
	}
	this->ListView->Items->BeginUpdate();
	this->ListView->Items->Clear();
	for(int unsigned i = 0; i < size; i++) showItem(a_orders[i]);
}
//---------------------------------------------------------------------------

void __fastcall TOrderManagmentForm::sortComboChange(TObject *Sender)
{
switch(this->sortCombo->ItemIndex){
	case 1:
		sortByStatus();
		break;
	case 2:
		sortByDev();
		break;
	case 3:
		sortByModel();
		break;
	case 4:
		sortBySn();
		break;
	case 5:
		sortByCustId();
		break;
	default:
		sortById();
}
}
//---------------------------------------------------------------------------

void __fastcall TOrderManagmentForm::editBtClick(TObject *Sender)
{
        int index = this->ListView->ItemIndex;
        int id = this->ListView->Items->operator [](index)->Caption.ToInt();
        int n = MainWindowForm->searchByIdO(id, orders);

        TNewOrderForm* form = new TNewOrderForm(this);
        form->setOrder(&MainWindowForm->orders->at(n));
        form->Show();
}
//---------------------------------------------------------------------------

void __fastcall TOrderManagmentForm::deleteBtClick(TObject *Sender)
{
        int index = this->ListView->ItemIndex;
        int id = this->ListView->Items->operator [](index)->Caption.ToInt();
        int n = MainWindowForm->searchByIdO(id, orders);
        MainWindowForm->orders->erase(MainWindowForm->orders->begin()+n);
        showAll();
}
//---------------------------------------------------------------------------

void __fastcall TOrderManagmentForm::refershClick(TObject *Sender)
{
showAll();        
}
//---------------------------------------------------------------------------

