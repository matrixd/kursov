# -*- coding: utf-8 -*-

import random
import string

def generateClients():
	names = ["Милана Афанасьевна Илларионова", "Ефросиния Викторовна Баранова", "Юлия Валентиновна Воронова",
				"Михаил Богданович Зимин", "Пелагея Владимировна Попова", "Игорь Михайлович Авдеев",
				"Анна Николаевна Зимина", "Степан Макарович Лихарев", "Елизавета Трофимовна Евстафьева",
				"Борис Русланович Мусаков"]
				
	f = open("clients.txt", "w+")
	i = 0
	for name in names:
		line = str(i) + ";+7" + str(random.randint(100000000000000000,999999999999999999)) + ";" + name + "\n"
		i = i+1
		f.write(line)
	f.close()
	
def generateOrders():
	f = open("orders.txt", "w+")

	for i in range(50):
		#5;1;1;1;123123;524352.5;fdggf34;descr
		#sprintf(buf, "%d;%d;%d;%d;%d;%f;", order.getId(), order.getCustomerId(), order.getType(),
		#                              order.getStatus(), order.getDate(), order.getPrice());
		line = str(i) + ";" + str(random.randint(0,9)) + ";" + str(random.randint(0,2)) + ";" + str(random.randint(0,1)) + ";" + str(random.randint(1325397600,1353956260)) + ";" + str(random.uniform(0,99999)) + ".0;"
		line += ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(5)) + ";"
		line += ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(10)) + ";"
		line += ''.join(random.choice(string.ascii_uppercase) for x in range(100)) + "\n"
		f.write(line)
	f.close()

generateOrders()

