//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "CustomerWindow.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TCustomerWindowForm *CustomerWindowForm;
//---------------------------------------------------------------------------
__fastcall TCustomerWindowForm::TCustomerWindowForm(TComponent* Owner)
        : TForm(Owner)
{
	exists = false;
}

void TCustomerWindowForm::setCustomer(Customer *link){
	customer = link;
	exists = true;
	this->editName->Text = customer->getName();
	this->editPhone->Text = customer->getPhone();
}

//---------------------------------------------------------------------------

void __fastcall TCustomerWindowForm::SaveClick(TObject *Sender)
{
	if(!exists) customer = new Customer();
	customer->setName(this->editName->Text);
	customer->setPhoneNumber(this->editPhone->Text.c_str());
	if(!exists) MainWindowForm->customers->push_back(*customer);
        NewOrderForm->fillList();
        delete this;
}
//---------------------------------------------------------------------------
void __fastcall TCustomerWindowForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
	delete this;
}
//---------------------------------------------------------------------------
