//---------------------------------------------------------------------------
#pragma hdrstop

#include "OrderReader.h"
//---------------------------------------------------------------------------
OrderReader::OrderReader(ifstream *file){
	stfile = file;
}
OrderReader::OrderReader(ofstream *file){
        ofile = file;
}

vector<Order>* OrderReader::read(){
	vector<Order> *orders = new vector<Order>();
	if (stfile->is_open()){
		while(stfile->good()){
			char buf[256];
			stfile->getline(buf, 256);
			string str(buf);
			if(str.empty()) break;
			vector<string>* strs = new vector<string>;
			size_t begin = 0, prevpos = 0;
			while(prevpos!=string::npos){
				if(prevpos != 0) begin = prevpos+1;
				prevpos = str.find(";", prevpos+1);
				strs->push_back(str.substr(begin, prevpos-begin));
			}
			int id, customerId, type, status, date;
			float price;
			string sn, description;

			id = ::atoi(strs->at(0).c_str());
			customerId = ::atoi(strs->at(1).c_str());
			type = ::atoi(strs->at(2).c_str());
			status = ::atoi(strs->at(3).c_str());
			date = ::atoi(strs->at(4).c_str());
			price = ::atof(strs->at(5).c_str());
			Order order(id);
			order.setConsumer(customerId);
			order.setType(type);
			order.setStatus(status);
			order.setDate(date);
			order.setPrice(price);
			order.setModel(strs->at(6).c_str());
			order.setSn(strs->at(7).c_str());
			order.setDescription(strs->at(8).c_str());
			orders->push_back(order);
			delete strs;
		}
	}
	return orders;
}

void OrderReader::write(vector<Order> orders){
	if (ofile->is_open()){
		string line;
		for(int unsigned i = 0; i<orders.size(); i++){
			Order order = orders.at(i);
                        char buf[256];
                        sprintf(buf, "%d;%d;%d;%d;%d;%f;", order.getId(), order.getCustomerId(), order.getType(),
                                  order.getStatus(), order.getDate(), order.getPrice());
                        AnsiString str(buf);
                        str += order.getModel() + ";";
                        str += order.getSn() + ";";
		       	str += order.getDescr();
                        *ofile << str << "\n";
		}
	}
}

#pragma package(smart_init)
