//---------------------------------------------------------------------------

#ifndef OrdersManageH
#define OrdersManageH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include "SearchWindow.h"
#include "MainWindow.h"
#include "Order.h"
//---------------------------------------------------------------------------
class TOrderManagmentForm : public TForm
{
__published:	// IDE-managed Components
	TListView *ListView;
    TButton *editBt;
    TButton *deleteBt;
    TButton *refersh;
	TComboBox *sortCombo;
    TButton *searchBt;
	void __fastcall searchBtClick(TObject *Sender);
	void __fastcall sortComboChange(TObject *Sender);
    void __fastcall editBtClick(TObject *Sender);
    void __fastcall deleteBtClick(TObject *Sender);
    void __fastcall refershClick(TObject *Sender);
private:	// User declarations
	vector<Order> *orders;
    void sortById();
    void sortByModel();
    void sortBySn();
    void sortByCustId();
    void sortByDev();

public:		// User declarations
	__fastcall TOrderManagmentForm(TComponent* Owner);
    void searchById(int id);
    void showItems();
    void showItem(Order order);
    void showAll();
    void searchByName(AnsiString name);
    void searchByModel(AnsiString model);
    void searchBySn(AnsiString sn);
	void sortByStatus();
};
//---------------------------------------------------------------------------
extern PACKAGE TOrderManagmentForm *OrderManagmentForm;
//---------------------------------------------------------------------------
#endif
