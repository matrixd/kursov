object OrderManagmentForm: TOrderManagmentForm
  Left = 521
  Top = 359
  Width = 1159
  Height = 527
  Caption = 'Orders managmert'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ListView: TListView
    Left = 16
    Top = 40
    Width = 937
    Height = 369
    Columns = <
      item
        Caption = 'id'
        Width = 30
      end
      item
        Caption = #1059#1089#1090#1088#1086#1081#1089#1090#1074#1086
        Width = 100
      end
      item
        Caption = #1052#1086#1076#1077#1083#1100
        Width = 200
      end
      item
        Caption = 'S/n'
        Width = 250
      end
      item
        Caption = #1057#1090#1072#1090#1091#1089
        Width = 70
      end
      item
        Caption = 'id '#1079#1072#1082#1072#1079#1095#1080#1082#1072
        Width = 100
      end
      item
        Caption = #1047#1072#1082#1072#1079#1095#1080#1082
        Width = 150
      end>
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
  end
  object editBt: TButton
    Left = 968
    Top = 104
    Width = 75
    Height = 25
    Caption = 'editBt'
    TabOrder = 1
    OnClick = editBtClick
  end
  object deleteBt: TButton
    Left = 968
    Top = 144
    Width = 75
    Height = 25
    Caption = 'deleteBt'
    TabOrder = 2
    OnClick = deleteBtClick
  end
  object refersh: TButton
    Left = 968
    Top = 184
    Width = 75
    Height = 25
    Caption = 'refersh'
    TabOrder = 3
    OnClick = refershClick
  end
  object sortCombo: TComboBox
    Left = 968
    Top = 56
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    Text = 'Sort by'
    OnChange = sortComboChange
    Items.Strings = (
      #1087#1086' id('#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102')'
      #1087#1086' '#1089#1090#1072#1090#1091#1089#1091
      #1087#1086' '#1091#1089#1090#1088#1086#1081#1089#1090#1074#1091
      #1087#1086' '#1084#1086#1076#1077#1083#1080
      #1087#1086' '#1089#1077#1088#1080#1081#1085#1086#1084#1091' '#1085#1086#1084#1077#1088#1091
      #1087#1086' '#1079#1072#1082#1072#1079#1095#1080#1082#1091)
  end
  object searchBt: TButton
    Left = 968
    Top = 224
    Width = 75
    Height = 25
    Caption = 'searchBt'
    TabOrder = 5
    OnClick = searchBtClick
  end
end
