object ClientsForm: TClientsForm
  Left = 1135
  Top = 376
  Width = 568
  Height = 361
  Caption = 'ClientsForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object new_bt: TButton
    Left = 472
    Top = 24
    Width = 75
    Height = 25
    Caption = 'new_bt'
    TabOrder = 0
    OnClick = new_btClick
  end
  object edit_bt: TButton
    Left = 472
    Top = 64
    Width = 75
    Height = 25
    Caption = 'edit_bt'
    TabOrder = 1
    OnClick = edit_btClick
  end
  object del_bt: TButton
    Left = 472
    Top = 104
    Width = 75
    Height = 25
    Caption = 'del_bt'
    TabOrder = 2
    OnClick = del_btClick
  end
  object ListView: TListView
    Left = 16
    Top = 16
    Width = 425
    Height = 289
    Columns = <
      item
        Caption = 'id'
      end
      item
        Caption = #1060#1048#1054
        Width = 200
      end
      item
        Caption = #1090#1077#1083#1077#1092#1086#1085
        Width = 150
      end>
    ReadOnly = True
    RowSelect = True
    TabOrder = 3
    ViewStyle = vsReport
  end
end
