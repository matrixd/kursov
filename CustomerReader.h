//---------------------------------------------------------------------------
#ifndef CustomerReaderH
#define CustomerReaderH

#include <iostream>
#include <fstream>
#include <vector>
#include "Customer.h"

using namespace std;

class CustomerReader{
private:
	ifstream *stfile;
	ofstream *ofile;
public:
	CustomerReader(ifstream *file);
	CustomerReader(ofstream *file);
	vector<Customer>* read();
	void write(vector<Customer> customers);
};
//---------------------------------------------------------------------------
#endif
