//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("MainWindow.cpp", MainWindowForm);
USEFORM("OrderWindow.cpp", NewOrderForm);
USEFORM("CustomerWindow.cpp", Form3);
USEFORM("OrdersManage.cpp", OrderManagmentForm);
USEFORM("SearchWindow.cpp", SearchForm);
USEFORM("CustomersWindow.cpp", ClientsForm);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainWindowForm), &MainWindowForm);
		Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        catch (...)
        {
                 try
                 {
                         throw Exception("");
                 }
                 catch (Exception &exception)
                 {
                         Application->ShowException(&exception);
                 }
        }
        return 0;
}
//---------------------------------------------------------------------------
