//---------------------------------------------------------------------------

#ifndef OrderWindow
#define OrderWindow
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "Order.h"
#include "MainWindow.h"
#include "CustomerWindow.h"
#include <ComCtrls.hpp>
#include "systdate.h"
#include "OrdersManage.h"
//---------------------------------------------------------------------------
class TNewOrderForm : public TForm
{
__published:	// IDE-managed Components
	TEdit *editModel;
	TButton *Button1;
	TEdit *editSn;
	TLabel *Label1;
	TLabel *Label2;
	TMemo *Memo1;
	TLabel *Label3;
	TComboBox *ComboBox1;
	TLabel *Label4;
	TButton *saveBt;
	TListView *ListView;
	TLabel *Label5;
	TLabel *dateLabel;
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall saveBtClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
	Order *order;
	bool exists;
	void fillCombo();
public:		// User declarations
	__fastcall TNewOrderForm(TComponent* Owner);
	void setOrder(Order *orderObj); //fills up form with data from order obj
	void fillList(); //refreshes info about customers
};
//---------------------------------------------------------------------------
extern PACKAGE TNewOrderForm *NewOrderForm;
//---------------------------------------------------------------------------
#endif
