//---------------------------------------------------------------------------

#ifndef MainWindow
#define MainWindow
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <System.hpp>
#include "OrderReader.h"
#include "OrderWindow.h"
#include "Customer.h"
#include "CustomerReader.h"
#include "OrdersManage.h"
#include "CustomersWindow.h"
//---------------------------------------------------------------------------
class TMainWindowForm : public TForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TButton *Button2;
        TButton *Button3;
        TButton *Button4;
        void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
private:	// User declarations
	Order *order;
	bool exists;
public:		// User declarations
        __fastcall TMainWindowForm(TComponent* Owner);
        vector<Customer> *customers;
        vector<Order> *orders;
        static const AnsiString types[];
        static const AnsiString status[];
        int searchByIdO(unsigned int id, vector<Order> *v);
        int searchByIdC(unsigned int id, vector<Customer> *v);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainWindowForm *MainWindowForm;
//---------------------------------------------------------------------------
#endif
