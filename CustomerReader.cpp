//---------------------------------------------------------------------------
#pragma hdrstop

#include "CustomerReader.h"


CustomerReader::CustomerReader(ifstream *file){
	stfile = file;
}
CustomerReader::CustomerReader(ofstream *file){
        ofile = file;
}

vector<Customer>* CustomerReader::read(){
	vector<Customer> *customers = new vector<Customer>();
	if (stfile->is_open()){
		while(stfile->good()){
			char buf[256];
			stfile->getline(buf, 256);
			string str(buf);
			vector<string>* strs = new vector<string>;
			if(str.empty()) break;
			unsigned int id;
			char phone[20];
			AnsiString name;
			int first = str.find(";");
			strs->push_back(str.substr(0, first));
			int sec = str.find(";", first+1);
			strs->push_back(str.substr(first+1, sec-first));
			int third = str.find("\n", sec+1);
			strs->push_back(str.substr(sec+1, third-first));
			id = ::atoi(strs->at(0).c_str());
			strcpy(phone,strs->at(1).c_str());
			name = strs->at(2).c_str();
			Customer customer(id);
			AnsiString tmp = name;
			customer.setName(name);
			customer.setPhoneNumber(phone);
			customers->push_back(customer);
		}
	}
	return customers;
}

void CustomerReader::write(vector<Customer> customers){
	if (ofile->is_open()){
		for(int unsigned i = 0; i<customers.size(); i++){
			Customer c = customers.at(i);
                        char buf[256];
                        sprintf(buf, "%d;%20s;%s", c.getId(), c.getPhone(), c.getName());
                        *ofile << buf << "\n";
		}
	}
}

//---------------------------------------------------------------------------

#pragma package(smart_init)
