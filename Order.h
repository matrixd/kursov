//---------------------------------------------------------------------------
#ifndef OrderH
#define OrderH
//---------------------------------------------------------------------------
#include <System.hpp>

class Order{
private:
	unsigned int id;
	unsigned int customerId;
	unsigned int type;
	unsigned int date;
	AnsiString sn;
	AnsiString descr;
	AnsiString model;
	unsigned int status;
	float price;
public:
	static unsigned int lastId;
	Order(unsigned int idNumber);
	Order();
	void setConsumer(unsigned int idnumber);
	void setType(unsigned int typeId);
	void setSn(AnsiString deviceSn);
	void setDescription(AnsiString description);
	void setPrice(float value);
    void setDate(unsigned int datetime);
    void setStatus(unsigned int st);
    void setModel(AnsiString modelName);
	unsigned int getOrderId();
	unsigned int getCustomerId();
	unsigned int getType();
	unsigned int getId();
	const int getDate();
	AnsiString getSn();
	AnsiString getDescr();
	AnsiString getModel();
	unsigned int getStatus();
	float getPrice();
};

#endif
