//---------------------------------------------------------------------------
#ifndef CustomerH
#define CustomerH

#include <System.hpp>

class Customer{
private:
	unsigned int idNumber;
	AnsiString customerName;
	AnsiString phone;
	static unsigned int lastId;
public:
	Customer(unsigned int id);
	Customer();
	void setName(AnsiString name);
	void setPhoneNumber(char phoneNumber[20]);
	AnsiString getName();
	AnsiString getPhone();
	unsigned int getId();
};

//---------------------------------------------------------------------------
#endif
