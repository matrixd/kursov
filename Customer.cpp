//---------------------------------------------------------------------------


#pragma hdrstop

#include "Customer.h"

unsigned int Customer::lastId = -1;


Customer::Customer(){
	idNumber = ++lastId;
}
Customer::Customer(unsigned int id){
	idNumber = id;
	if(id > lastId) lastId = id;
}


void Customer::setName(AnsiString name){
	customerName = name;
}

void Customer::setPhoneNumber(char phoneNumber[20]){
	phone = phoneNumber;
}


AnsiString Customer::getName(){
	return customerName;
}

AnsiString Customer::getPhone(){
	return phone;
}

unsigned int Customer::getId(){
	return idNumber;
}

//---------------------------------------------------------------------------

#pragma package(smart_init)
