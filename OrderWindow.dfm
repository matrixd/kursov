object NewOrderForm: TNewOrderForm
  Left = 396
  Top = 347
  Width = 766
  Height = 564
  Caption = 'NewOrderForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 400
    Top = 88
    Width = 39
    Height = 13
    Caption = #1052#1086#1076#1077#1083#1100
  end
  object Label2: TLabel
    Left = 400
    Top = 128
    Width = 16
    Height = 13
    Caption = 's/n'
  end
  object Label3: TLabel
    Left = 376
    Top = 176
    Width = 97
    Height = 13
    Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1087#1086#1083#1086#1084#1082#1080
  end
  object Label4: TLabel
    Left = 400
    Top = 56
    Width = 79
    Height = 13
    Caption = #1058#1080#1087' '#1091#1089#1090#1088#1086#1081#1089#1090#1074#1072
  end
  object Label5: TLabel
    Left = 400
    Top = 24
    Width = 106
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1086#1076#1072#1095#1080' '#1079#1072#1103#1074#1082#1080':'
  end
  object dateLabel: TLabel
    Left = 520
    Top = 24
    Width = 47
    Height = 13
    Caption = 'dateLabel'
  end
  object editModel: TEdit
    Left = 488
    Top = 88
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'editModel'
  end
  object Button1: TButton
    Left = 56
    Top = 32
    Width = 105
    Height = 25
    Caption = #1053#1086#1074#1099#1081' '#1079#1072#1082#1072#1079#1095#1080#1082
    TabOrder = 1
    OnClick = Button1Click
  end
  object editSn: TEdit
    Left = 488
    Top = 128
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'editSn'
  end
  object Memo1: TMemo
    Left = 376
    Top = 200
    Width = 345
    Height = 273
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
  end
  object ComboBox1: TComboBox
    Left = 488
    Top = 48
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    Text = 'ComboBox1'
  end
  object saveBt: TButton
    Left = 648
    Top = 480
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 5
    OnClick = saveBtClick
  end
  object ListView: TListView
    Left = 48
    Top = 72
    Width = 200
    Height = 441
    Columns = <
      item
        Caption = #1060#1048#1054
        Width = 195
      end
      item
        Caption = 'id'
        Width = 0
      end>
    ReadOnly = True
    RowSelect = True
    TabOrder = 6
    ViewStyle = vsReport
  end
end
