object CustomerWindowForm: TCustomerWindowForm
  Left = 252
  Top = 234
  Width = 218
  Height = 233
  Caption = 'CustomerWindowForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object editName: TEdit
    Left = 40
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'editName'
  end
  object Save: TButton
    Left = 56
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 1
    OnClick = SaveClick
  end
  object editPhone: TEdit
    Left = 40
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'editPhone'
  end
end
