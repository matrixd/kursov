//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CustomersWindow.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TClientsForm *ClientsForm;
//---------------------------------------------------------------------------
__fastcall TClientsForm::TClientsForm(TComponent* Owner)
	: TForm(Owner)
{
	refresh();
}
void TClientsForm::refresh(){
	int size = int(MainWindowForm->customers->size());
	Customer *a_clients = new Customer[size];
	for(int unsigned i = 0; i < size; i++){
		a_clients[i] = MainWindowForm->customers->at(i);
		Customer tmp = a_clients[i];
		int j;
		for(j = i; j > 0 && tmp.getName() < a_clients[j-1].getName(); j--)
			a_clients[j] = a_clients[j-1];
			a_clients[j] = tmp;
        }
        this->ListView->Items->BeginUpdate();
        this->ListView->Items->Clear();
        for(int unsigned i = 0; i < size; i++) {
        	TListItem * item;
                item = this->ListView->Items->Add();
                item->Caption = a_clients[i].getId();
                item->SubItems->Add(a_clients[i].getName());
                item->SubItems->Add(a_clients[i].getPhone());
        }
        this->ListView->Items->EndUpdate();
}
//---------------------------------------------------------------------------
void __fastcall TClientsForm::new_btClick(TObject *Sender)
{
	TCustomerWindowForm *form = new TCustomerWindowForm(this);
        form->Show();		
}
//---------------------------------------------------------------------------

void __fastcall TClientsForm::edit_btClick(TObject *Sender)
{
	TCustomerWindowForm *form = new TCustomerWindowForm(this);
        int id = ListView->Items->operator [](ListView->ItemIndex)->Caption.ToInt();
        int index = MainWindowForm->searchByIdC(id, MainWindowForm->customers);
        form->setCustomer(&MainWindowForm->customers->at(index));
        form->Show();
}
//---------------------------------------------------------------------------


void __fastcall TClientsForm::del_btClick(TObject *Sender)
{
	int index = this->ListView->ItemIndex;
        int id = this->ListView->Items->operator [](index)->Caption.ToInt();
        int n = MainWindowForm->searchByIdC(id, MainWindowForm->customers);
        MainWindowForm->customers->erase(MainWindowForm->customers->begin()+n);
        refresh();
}
//---------------------------------------------------------------------------

void __fastcall TClientsForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
	delete this;
}
//---------------------------------------------------------------------------

