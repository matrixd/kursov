//---------------------------------------------------------------------------
#pragma hdrstop

#include "Order.h"
//---------------------------------------------------------------------------

unsigned int Order::lastId = 0;

Order::Order(unsigned int idNumber){
	id = idNumber;
	if(id > lastId) Order::lastId = id;
}
Order::Order(){
	Order::lastId = Order::lastId + 1;
	id = Order::lastId;
	status = 0;
}
void Order::setConsumer(unsigned int idNumber){
	customerId = idNumber;
}
void Order::setType(unsigned int typeId){
	type = typeId;
}
void Order::setSn(AnsiString deviceSn){
	sn = deviceSn;
}
void Order::setDescription(AnsiString description){
	descr = description;
}
void Order::setPrice(float value){
	price = value;
}
void Order::setDate(unsigned int datetime){
	date = date;
}
void Order::setStatus(unsigned int st){
	status = st;
}
void Order::setModel(AnsiString modelName){
	model = modelName;
}

unsigned int Order::getOrderId(){
	return id;
}
unsigned int Order::getCustomerId(){
	return customerId;
}
unsigned int Order::getType(){
	return type;
}
AnsiString Order::getSn(){
	return sn;
}
AnsiString Order::getDescr(){
	return descr;
}
AnsiString Order::getModel(){
	return model;
}
unsigned int Order::getStatus(){
	return status;
}
float Order::getPrice(){
	return price;
}
unsigned int Order::getId(){
	return id;
}
const int Order::getDate(){
	return date;
}

#pragma package(smart_init)
