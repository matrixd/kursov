//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "OrderWindow.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TNewOrderForm *NewOrderForm;
//---------------------------------------------------------------------------
__fastcall TNewOrderForm::TNewOrderForm(TComponent* Owner)
        : TForm(Owner)
{
	exists = false;
        fillList();
        fillCombo();
}

void TNewOrderForm::setOrder(Order *orderObj){
	order = orderObj;
	exists = true;
	this->editSn->Text = order->getSn();
	this->editModel->Text = order->getModel();
        TDateTime a(order->getDate());
        this->dateLabel->Caption = a.DateString();
	this->ComboBox1->ItemIndex = order->getType();
	int index = MainWindowForm->searchByIdC(order->getCustomerId(), MainWindowForm->customers);
        this->ListView->Items->operator [](index)->Selected = true;
	this->Memo1->Text = order->getDescr();
}


void TNewOrderForm::fillList(){
	TListItem * item;
        this->ListView->Items->BeginUpdate();
        this->ListView->Items->Clear();
        
	for(int unsigned i = 0; i < MainWindowForm->customers->size(); i++){
		Customer c = MainWindowForm->customers->at(i);
		AnsiString buf = c.getName();
		item = this->ListView->Items->Add();
		item->Caption = buf;
                item->SubItems->Add(c.getId());
	}
	this->ListView->Items->EndUpdate();
}
void TNewOrderForm::fillCombo(){
	for(int i = 0; i < 3; i++) this->ComboBox1->AddItem(TMainWindowForm::types[i], this);
}

//---------------------------------------------------------------------------

void __fastcall TNewOrderForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        delete this;
}
//---------------------------------------------------------------------------

void __fastcall TNewOrderForm::saveBtClick(TObject *Sender)
{
	if(!exists){
		order = new Order();
	}
        order->setDescription(this->Memo1->Text);
        order->setType(this->ComboBox1->ItemIndex);
        order->setModel(this->editModel->Text);
        order->setSn(this->editSn->Text);
        order->setConsumer(this->ListView->Selected->SubItems->operator [](0).ToInt());
        if(!exists) MainWindowForm->orders->push_back(*order);
        delete this;
}
//---------------------------------------------------------------------------

void __fastcall TNewOrderForm::Button1Click(TObject *Sender)
{
	TCustomerWindowForm *form = new TCustomerWindowForm(this);
        form->Show();
}
//---------------------------------------------------------------------------

