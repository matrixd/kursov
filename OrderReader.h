//---------------------------------------------------------------------------
#ifndef OrderReaderH
#define OrderReaderH

#include <iostream>
#include <fstream>
#include <vector>
#include "Order.h"

using namespace std;

class OrderReader{
private:
	ifstream *stfile;
        ofstream *ofile;
public:
	OrderReader(ifstream *file);
        OrderReader(ofstream *file);
	vector<Order>* read();
	void write(vector<Order> orders);
};

//---------------------------------------------------------------------------
#endif
