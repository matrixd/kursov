//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainWindow.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainWindowForm *MainWindowForm;
//---------------------------------------------------------------------------
int TMainWindowForm::searchByIdO(unsigned int id, vector<Order> *v){
	int index;
	for(int i = 0; i < v->size(); i++){
		if(v->at(i).getId() == id){
			index = i;
			break;
		}
	}
	return index;
}

int TMainWindowForm::searchByIdC(unsigned int id, vector<Customer> *v){
	int index;
	for(int i = 0; i < v->size(); i++){
		if(v->at(i).getId() == id){
			index = i;
			break;
		}
	}
	return index;
}

const AnsiString TMainWindowForm::types[] = {"�����������", "������", "�������"};
const AnsiString TMainWindowForm::status[] = {"��������", "� ��������", "������"};
__fastcall TMainWindowForm::TMainWindowForm(TComponent* Owner)
        : TForm(Owner)
{

ifstream *ifs = new ifstream( "orders.txt" , ifstream::in );
OrderReader *reader = new OrderReader(ifs);
orders = reader->read();

delete reader;
ifs->close();
delete ifs;

ifs = new ifstream("clients.txt", ifstream::in);
CustomerReader *creader = new CustomerReader(ifs);
customers = creader->read();
delete creader;
ifs->close();
delete ifs;
}
//---------------------------------------------------------------------------


void __fastcall TMainWindowForm::Button1Click(TObject *Sender)
{
        TNewOrderForm *form = new TNewOrderForm(this);
        form->Show();
}
//---------------------------------------------------------------------------

void __fastcall TMainWindowForm::Button2Click(TObject *Sender)
{
	TOrderManagmentForm *form = new TOrderManagmentForm(this);
        form->Show();
}
//---------------------------------------------------------------------------


void __fastcall TMainWindowForm::Button4Click(TObject *Sender)
{
	TClientsForm* form = new TClientsForm(this);
        form->Show();
}
//---------------------------------------------------------------------------

